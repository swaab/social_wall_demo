<?php
/**
 * @file
 * social_wall_integration.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function social_wall_integration_image_default_styles() {
  $styles = array();

  // Exported image style: sw_post_picture.
  $styles['sw_post_picture'] = array(
    'label' => 'sw_post_picture',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 500,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sw_profile_picture.
  $styles['sw_profile_picture'] = array(
    'label' => 'sw_profile_picture',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 64,
          'height' => 64,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
